# Band App

This is Band App. I played arround a little to make this more interesting, hope you will like it.

## Interestring features

- Basic Offline support (last artist and events are kept in local storage, and for files I use service workers)

- Basic tests, more interesting one are for Modal and BandStore

- Typescript

- BIO (BEM, ITCSS, OOCSS) + Inuit

- Basic Google Maps support

- Preload of Google Fonts using html preload, they are later added using JS. It makes for faster initial load

- State managment - this could be done using pure React state, but I decided to use MobX and new Context for core data, to make it more interesting. Some componets still use normal state, where adding external state managment would be an overkill.
