const isDevelopment = process.env.NODE_ENV === 'development';
const devUrl = 'http://localhost:3000';
const appUrl = 'https://band-app.netlify.com/';
const baseUrl = isDevelopment ? devUrl : appUrl;

const ENVIRONMENT = {
  baseUrl,
  bandsInTown: {
    id: 'thisIsTestId'
  }
};

export { ENVIRONMENT };
