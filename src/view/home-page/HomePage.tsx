import React, { SFC } from 'react';
import { RouteComponentProps } from 'react-router';
import ErrorBoundary from '../../components/ErrorBoundary';
import BandInput from './BandInput';
import BandCard from './BandCard';
import Events from './Events';

const HomePage: SFC<RouteComponentProps<{}>> = () => (
  <ErrorBoundary>
    <div className="o-wrapper  o-box">
      <header>
        <h1>Find the band</h1>
      </header>
      <main className="o-layout  o-layout--strech">
        <section className="o-layout__item  u-1/1  u-1/3@desktop">
          <BandInput />
          <BandCard />
        </section>
        <section className="o-layout__item  u-1/1  u-2/3@desktop">
          <Events />
        </section>
      </main>
    </div>
  </ErrorBoundary>
);

export default HomePage;
