import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Event as BandEvent } from '../../../modules/api/getEvents';
import Card from '../../../components/card/Card';
import Modal from '../../../components/modal/Modal';
import Map from '../../../components/map/Map';
import BasicDecription from './BasicDescription';

interface EventProps {
  event: BandEvent;
}

interface EventState {
  expanded: boolean;
}

export class Event extends Component<EventProps, EventState> {
  state = {
    expanded: false
  };

  render() {
    const { event } = this.props;
    const { description } = event;

    return (
      <>
        <div className="o-layout__item  u-1/1  u-1/2@tablet">
          <Card onClick={this.toggleCard}>
            <BasicDecription event={event} />
          </Card>
        </div>
        {this.state.expanded && (
          <Modal
            closeOnEsc
            closeOnOutsideClick
            className="c-modal"
            onClose={this.toggleCard}
          >
            <Card>
              <BasicDecription event={event} />
              <div className="o-column  u-margin-small">
                <p>{description}</p>
              </div>
              <Map
                latitude={event.venue.latitude}
                longitude={event.venue.longitude}
                containerElement={CONTAINER_NODE}
                mapElement={CONTAINER_NODE}
              />
            </Card>
          </Modal>
        )}
      </>
    );
  }

  private toggleCard = () =>
    this.setState(state => ({ expanded: !state.expanded }));
}

const CONTAINER_STYLES = { height: `400px` };
const CONTAINER_NODE = <div style={CONTAINER_STYLES} />;

export default observer(Event);
