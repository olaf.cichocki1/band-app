import React, { SFC } from 'react';
import moment from 'moment';
import classNames from 'classnames';
import { Event } from '../../../modules/api/getEvents';

interface EventProps {
  event: Event;
}

const BasicDecription: SFC<EventProps> = ({ event }) => {
  const { venue, datetime } = event;
  const date = moment(datetime);
  const month = date.format('MMM');
  const day = date.format('DD');
  const daysToEvent = moment().diff(datetime, 'days');

  return (
    <div className="o-row  o-align-center">
      <div
        className={classNames('c-calendar-card  u-margin-small', {
          'c-calendar-card--soon': daysToEvent >= -7,
          'c-calendar-card--today': daysToEvent === 0
        })}
      >
        <div className="c-calendar-card__day">{day}</div>
        <div className="c-calendar-card__month">{month}</div>
      </div>
      <div className="o-column  u-margin-left-auto  u-margin-small">
        <div>{venue.name}</div>
        <div>
          {venue.city}, {venue.country}
        </div>
      </div>
    </div>
  );
};

export default BasicDecription;
