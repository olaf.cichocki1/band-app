import React, { Component, RefObject } from 'react';
import { observer } from 'mobx-react';
import debounce from 'lodash.debounce';
import { StoreContext } from '../../store';
import ErrorBoundary from '../../components/ErrorBoundary';
import Card from '../../components/card/Card';

export class BandInput extends Component {
  static contextType = StoreContext;
  context!: React.ContextType<typeof StoreContext>;

  render() {
    return (
      <ErrorBoundary>
        <Card className="o-box">
          <label htmlFor="bandName" className="h4">
            Input the band name:
          </label>
          <input
            id="bandName"
            value={this.context.band.searchQuery}
            onChange={this.onInputChange}
          />
        </Card>
      </ErrorBoundary>
    );
  }

  private onInputChange = (event: React.FormEvent<HTMLInputElement>) => {
    const { value } = event.currentTarget;
    this.context.band.updateSearchQuery(value);
    this.updateBandDebounced(value);
  };

  private updateBandDebounced = debounce(this.updateBand, 250, {
    leading: true,
    trailing: true,
    maxWait: 500
  });

  private updateBand(value: string) {
    this.context.band.updateBand(value);
  }
}

export default observer(BandInput);
