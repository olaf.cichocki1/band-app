import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { StoreContext } from '../../store';
import { Loader } from '../../components/loader/Loader';
import Card from '../../components/card/Card';
import facebookLogo from './img/facebook.svg';

export class BandCard extends Component {
  static contextType = StoreContext;
  context!: React.ContextType<typeof StoreContext>;

  render() {
    const { artist, searchQuery, loading } = this.context.band;

    if (!artist || !searchQuery) {
      return null;
    }

    return (
      <Card>
        <div>
          {!loading && artist.image_url && (
            <div className="u-text-center">
              <img src={artist.image_url} />
            </div>
          )}
          {loading && <Loader size={250} className="u-center-block" />}
        </div>
        <div className="o-box">
          <div>
            <h2 className="u-h3">{artist.name}</h2>
          </div>
          <div>Upcoming events: {artist.upcoming_event_count}</div>
          {artist.facebook_page_url && (
            <div className="u-margin-top-small">
              <a
                href={artist.facebook_page_url}
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={facebookLogo} />
              </a>
            </div>
          )}
        </div>
      </Card>
    );
  }
}

export default observer(BandCard);
