import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { StoreContext } from '../../store';
import { Event } from './event/Event';

export class Events extends Component {
  static contextType = StoreContext;
  context!: React.ContextType<typeof StoreContext>;

  render() {
    const { events } = this.context.band;

    if (!events || events.length === 0) {
      return null;
    }

    return (
      <div className="o-layout">
        {events.map(event => (
          <Event key={event.id} event={event} />
        ))}
      </div>
    );
  }
}

export default observer(Events);
