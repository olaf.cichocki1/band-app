import React from 'react';
import { mount } from 'enzyme';
import Modal from './Modal';

describe('<Modal>', () => {
  describe('handles properly onClose', () => {
    it('X icon', () => {
      const onClose = jest.fn();
      const component = mount(<Modal onClose={onClose}>Some content</Modal>);
      component.find('.qa-modal-close-icon').simulate('click');
      expect(onClose).toHaveBeenCalled();
    });

    it('Should not call action on click inside the component', () => {
      const onClose = jest.fn();
      const component = mount(<Modal onClose={onClose} closeOnOutsideClick />);
      component.simulate('click');

      expect(onClose).not.toHaveBeenCalled();
    });

    it('Should call action on click outside the component', () => {
      const map: { [key: string]: any } = {};
      const onClose = jest.fn();

      document.addEventListener = jest.fn((event: string, cb) => {
        map[event] = cb;
      });

      const component = mount(
        <div>
          <div id="decoy">Decoy</div>
          <Modal onClose={onClose} closeOnOutsideClick />
        </div>
      );

      // We use stub, since testing using simulated click does not work
      map.click({
        target: document.getElementById('decoy')
      });

      expect(onClose).toHaveBeenCalled();
    });

    it('Should call action on ESC', () => {
      const map: { [key: string]: any } = {};
      const onClose = jest.fn();

      document.addEventListener = jest.fn((event: string, cb) => {
        map[event] = cb;
      });

      const component = mount(<Modal onClose={onClose} closeOnEsc />);

      map.keydown({ keyCode: 13 }); // Enter
      expect(onClose).not.toHaveBeenCalled();

      map.keydown({ keyCode: 27 }); // Esc
      expect(onClose).toHaveBeenCalled();
    });
  });

  it('Accepts external classes', () => {
    const onClose = jest.fn();
    const component = mount(
      <Modal onClose={onClose} className="test-class-name">
        Some content
      </Modal>
    );
    expect(component.find('.test-class-name').length).toBeTruthy();
  });
});
