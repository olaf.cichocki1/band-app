import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import KeyCode from '../KeyCode';
import CloseIcon, { CloseIconProps } from './components/CloseIcon';

export interface ModalProps extends CloseIconProps {
  /** Function fired to close Modal */
  onClose: () => void;
  /** Allows to fire onClose on ESC */
  closeOnEsc?: boolean;
  /** Allows to fire onClose on click outside the modal */
  closeOnOutsideClick?: boolean;
  className?: string;
}

/**
 * Modal uses React 16 portal to appear in the different DOM node.
 * If You provide it with node, it will portal inside, otherwise
 * it will mount itself inside the div at the end of the `<body>`
 *
 * If You provide custom node, the overlay will be disabled.
 */
export class Modal extends Component<ModalProps> {
  private modal: React.RefObject<HTMLDivElement> = React.createRef();
  private defaultNode: HTMLElement | null = null;

  componentWillMount() {
    this.prepareDOM();
  }

  componentDidMount() {
    if (this.props.closeOnEsc) {
      document.addEventListener('keydown', this.onKeydown);
    }
    if (this.props.closeOnOutsideClick) {
      document.addEventListener('click', this.onOutsideModalClick);
    }
  }

  componentWillUpdate() {
    this.prepareDOM();
  }

  componentWillUnmount() {
    if (this.props.closeOnEsc) {
      document.removeEventListener('keydown', this.onKeydown);
    }
    if (this.props.closeOnOutsideClick) {
      document.removeEventListener('click', this.onOutsideModalClick);
    }
    if (this.defaultNode) {
      document.body.removeChild(this.defaultNode);
    }
  }

  render() {
    const { children, className, onClose } = this.props;

    return ReactDOM.createPortal(
      <div ref={this.modal} className={className}>
        <CloseIcon onClose={onClose} />
        <div>{children}</div>
      </div>,
      this.defaultNode!
    );
  }

  private prepareDOM = (): void => {
    if (!this.defaultNode) {
      this.defaultNode = document.createElement('div');
      this.defaultNode.className = 'c-modal__overlay';
      document.body.appendChild(this.defaultNode);
    }
  };

  private onOutsideModalClick = (event: MouseEvent): void => {
    const isNonPrimaryButtonPressed: boolean =
      event.button !== undefined && event.button !== 0;

    if (
      !this.modal.current ||
      this.modal.current.contains(event.target as HTMLElement) ||
      isNonPrimaryButtonPressed
    ) {
      return;
    }

    if (this.props.onClose) {
      this.props.onClose();
    }
  };

  private onKeydown = (event: KeyboardEvent): void => {
    if (event.keyCode === KeyCode.Escape && this.props.onClose) {
      this.props.onClose();
    }
  };
}

export default Modal;
