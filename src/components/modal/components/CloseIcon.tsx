import React, { SFC } from 'react';

export interface CloseIconProps {
  /** Function fired to close Modal */
  onClose: () => void;
  color?: 'string';
}

const CloseIcon: SFC<CloseIconProps> = ({ onClose, color = '#5472d3' }) => (
  <div className="c-modal__icon  qa-modal-close-icon" onClick={onClose}>
    <svg width="20" height="26" viewBox="0 0 23 22">
      <path
        fill="none"
        fillRule="evenodd"
        stroke={color}
        strokeLinecap="square"
        strokeOpacity="0.5"
        strokeWidth="1"
        d="M11.284 11.284l10.607 10.607-10.607-10.607L21.891.678 11.284 11.284zm0 0L.678.678l10.606 10.606L.678 21.891l10.606-10.607z"
      />
    </svg>
  </div>
);

export default CloseIcon;
