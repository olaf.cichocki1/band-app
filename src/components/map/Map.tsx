import React, { SFC } from 'react';
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

interface MapProps {
  latitude: string;
  longitude: string;
}

// TODO: Add tests
const Map: SFC<MapProps> = ({ latitude, longitude }) => {
  const lat = Number(latitude);
  const lng = Number(longitude);

  return (
    <GoogleMap defaultZoom={8} defaultCenter={{ lat, lng }}>
      <Marker position={{ lat, lng }} />
    </GoogleMap>
  );
};

export default withGoogleMap(Map);
