import React from 'react';
import TestRenderer from 'react-test-renderer';
import Loader from './Loader';

describe('<Loader>', () => {
  it('Renders correctly with defaults', () => {
    const component = TestRenderer.create(<Loader />);
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('Renders correctly with size props', () => {
    const component = TestRenderer.create(<Loader size={150} />);
    expect(component.toJSON()).toMatchSnapshot();
  });
});
