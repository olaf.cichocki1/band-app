import React, { SFC } from 'react';
import loading from './loading.svg';

export interface LoaderProps {
  size?: number;
  className?: string;
}

export const Loader: SFC<LoaderProps> = ({ size = 100, className }) => (
  <div className={className} style={{ width: size, height: size }}>
    <img src={loading} />
  </div>
);

export default React.memo(Loader);
