import React from 'react';
import TestRenderer from 'react-test-renderer';
import { mount } from 'enzyme';
import Card from './Card';

describe('<Card>', () => {
  it('Renders correctly with defaults', () => {
    const component = TestRenderer.create(<Card>Content</Card>);
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('Renders correctly when mini card is enabled', () => {
    const component = TestRenderer.create(<Card mini>Content</Card>);
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('Works correctly when clickable', () => {
    const onClick = jest.fn();
    const component = mount(
      <Card mini onClick={onClick}>
        Content
      </Card>
    );

    expect(component.find('.c-card--clickable').length).toBeTruthy();
    component.simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
