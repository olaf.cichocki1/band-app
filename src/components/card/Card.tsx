import React, { SFC } from 'react';
import ErrorBoundary from '../../components/ErrorBoundary';
import classNames from 'classnames';

export interface CardProps {
  /** Restrict width to 300px */
  mini?: boolean;
  /** Pass custom class to outer div */
  className?: string;
  /** Function to call on Card click, also adds hover styles */
  onClick?: () => void;
}

const Card: SFC<CardProps> = ({
  children = null,
  className,
  mini,
  onClick
}) => (
  <div
    className={classNames(className, 'c-card  qa-card', {
      'c-card--mini': mini,
      'c-card--clickable': Boolean(onClick)
    })}
    onClick={onClick}
  >
    <ErrorBoundary>{children}</ErrorBoundary>
  </div>
);

export default Card;
