// We are exporting it like this so we keep isolatedModules enabled

export default (function() {
  let styles = document.createElement('link');
  styles.rel = 'stylesheet';
  styles.type = 'text/css';
  styles.href =
    'https://fonts.googleapis.com/css?family=Lato:400,700&amp;subset=latin-ext';
  document.getElementsByTagName('head')[0].appendChild(styles);
})();
