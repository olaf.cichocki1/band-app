import { ENVIRONMENT } from '../../settings';
const apiId = ENVIRONMENT.bandsInTown.id;

export type Offer = {
  status: string;
  type: string;
  url: string;
};

export type Event = {
  artist_id: string;
  datetime?: string;
  description?: string;
  id: string;
  lineup: string[];
  offers: Offer[];
  on_sale_datetime?: string;
  url: string;
  venue: {
    country: string;
    city: string;
    latitude: string;
    longitude: string;
    name: string;
    region: string;
  };
};

type ErrorMessage = {
  errorMessage: string;
};

type apiResponse = Event[] | ErrorMessage;

export const getEvents = (artist: string = '', date: string = 'upcoming') =>
  fetch(
    `https://rest.bandsintown.com/artists/${artist}/events?app_id=${apiId}&date=${date}`
  )
    .then(reponse => reponse.json())
    .then((reponse: apiResponse) => {
      if (reponse.hasOwnProperty('errorMessage')) {
        return [];
      }

      return reponse as Event[];
    })
    .catch(error => []);

export default getEvents;
