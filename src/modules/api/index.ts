import { getBand } from './getBand';
import { getEvents } from './getEvents';

export class API {
  getBand = getBand;
  getEvents = getEvents;
}

export default new API();
