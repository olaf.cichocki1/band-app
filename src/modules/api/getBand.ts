import { ENVIRONMENT } from '../../settings';
const apiId = ENVIRONMENT.bandsInTown.id;

export type Band = {
  facebook_page_url: string;
  image_url: string;
  mbid: string;
  name: string;
  thumb_url: string;
  tracker_count: number;
  upcoming_event_count: number;
  url: string;
};

export const getBand = (artist: string = '') =>
  fetch(`https://rest.bandsintown.com/artists/${artist}?app_id=${apiId}`)
    .then(reponse => reponse.json())
    .then((reponse: Band) => reponse);
