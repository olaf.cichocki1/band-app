import React, { Component } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router';
import { observer } from 'mobx-react';
import history from './history';
import HomePage from './view/home-page/HomePage';
import './preloaded';
import './css/App.scss';
import Store, { StoreProvider } from './store';

export class App extends Component {
  render() {
    return (
      <StoreProvider value={Store}>
        <Router history={history}>
          <Switch>
            <Route path="/" component={HomePage} />
            <Redirect to="/" />
          </Switch>
        </Router>
      </StoreProvider>
    );
  }
}

export default observer(App);
