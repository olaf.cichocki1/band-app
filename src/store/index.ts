import BandStore from './BandStore';
import API from '../modules/api';
import { createContext } from 'react';

class RootStore {
  band = new BandStore(this, API);
}

const singleton = new RootStore();
const StoreContext = createContext(singleton);
const StoreProvider = StoreContext.Provider;

export { RootStore, StoreContext, StoreProvider };
export default singleton;
