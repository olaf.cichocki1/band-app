import { RootStore } from '.';
import { observable, action } from 'mobx';
import { API } from '../modules/api';
import { Event } from '../modules/api/getEvents';
import { Band } from '../modules/api/getBand';
import { localStored } from 'mobx-stored';

const DEFAULT_BAND: Band = {
  facebook_page_url: '',
  image_url: '',
  mbid: '',
  name: '',
  thumb_url: '',
  tracker_count: 0,
  upcoming_event_count: 0,
  url: ''
};

const DEFAULT_EVENTS: { list: Event[] } = {
  list: []
};

class BandStore {
  root: RootStore;
  api: API;
  @observable searchQuery: string = '';
  artist: Band = localStored('band', DEFAULT_BAND, { delay: 0 });
  bandEvents = localStored('events', DEFAULT_EVENTS, { delay: 0 });
  get events(): Event[] {
    return this.bandEvents.list;
  }
  @observable loading: boolean = false;

  constructor(root: RootStore, api: API) {
    this.root = root;
    this.api = api;

    if (this.artist.name) {
      this.updateBand(this.artist.name);
      this.updateSearchQuery(this.artist.name);
    }
  }

  @action.bound
  updateSearchQuery(query: string = '') {
    this.searchQuery = query;
  }

  @action.bound
  async updateBand(bandName: string = '') {
    this.loading = true;

    const [band, events] = await Promise.all([
      this.api.getBand(bandName),
      this.api.getEvents(bandName)
    ]);

    this._updateBandData(band, events);
  }

  // Private
  @action.bound
  _updateBandData(band: any, events: any[]) {
    Object.assign(this.artist, band);
    Object.assign(this.bandEvents.list, events);
    this.loading = false;
  }
}

export default BandStore;
