import BandStore from './BandStore';
import { RootStore } from '.';
import { API } from '../modules/api';
import { Event } from '../modules/api/getEvents';
import { toJS } from 'mobx';
import MockAPI, {
  BandResponseMock,
  EventsResponseMock
} from './__mocks__/api.mock';

class RootMock {}
const RootStoreMock = new RootMock() as RootStore;

describe('BandStore', () => {
  it('Save fetched data', async () => {
    const store = new BandStore(RootStoreMock, MockAPI);
    await store.updateBand('Metallica');
    const artist = toJS(store.artist);
    const events = toJS(store.events);

    // Remove properties added by external libraries
    delete (artist as any).destroy;
    delete (artist as any).extend;
    delete (artist as any).reset;
    delete (events as any).destroy;
    delete (events as any).extend;
    delete (events as any).reset;

    expect(artist).toEqual(BandResponseMock);
    expect(events).toEqual(EventsResponseMock);
  });

  it('Updates query properly', async () => {
    const store = new BandStore(RootStoreMock, MockAPI);
    store.updateSearchQuery('Metallica');

    expect(store.searchQuery).toEqual('Metallica');
  });
});
