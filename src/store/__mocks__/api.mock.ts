import { API } from '../../modules/api';
import { Event } from '../../modules/api/getEvents';

const APIMock: API = {
  getBand: () => Promise.resolve(BandResponseMock),
  getEvents: () => Promise.resolve(EventsResponseMock)
};

export const BandResponseMock = {
  facebook_page_url: '',
  id: '128',
  image_url: 'https://s3.amazonaws.com/bit-photos/large/6874519.jpeg',
  mbid: '65f4f0c5-ef9e-490c-aee3-909e7ae6b2ab',
  name: 'Metallica',
  thumb_url: 'https://s3.amazonaws.com/bit-photos/thumb/6874519.jpeg',
  tracker_count: 3628967,
  upcoming_event_count: 43,
  url: 'https://www.bandsintown.com/a/128?came_from=267&app_id=xxx.xxx'
};

export const EventsResponseMock: Event[] = [
  {
    artist_id: '128',
    datetime: '2018-12-05T19:30:00',
    description:
      'Security Measures - No bags larger than 14x14x6 will be allowed inside the venue. All bags will be searched, please have all zippers and pouches on bags open and ready for inspection. In addition, all guests will go through metal detector scans. Please remove cell phones, keys and all metal from pockets prior to search. Remember, pocket knives are not allowed in the building.',
    id: '1007406590',
    lineup: ['Metallica', 'Jim Breuer'],
    offers: [
      {
        status: 'available',
        type: 'Tickets',
        url:
          'https://www.bandsintown.com/t/1007406590?app_id=xxx.xxx&came_from=267&utm_medium=api&utm_source=public_api&utm_campaign=ticket'
      }
    ],
    on_sale_datetime: '2018-03-02T18:00:00',
    url:
      'https://www.bandsintown.com/e/1007406590?app_id=xxx.xxx&came_from=267&utm_medium=api&utm_source=public_api&utm_campaign=event',
    venue: {
      city: 'Portland',
      country: 'United States',
      latitude: '45.531982',
      longitude: '-122.667838',
      name: 'Moda Center',
      region: 'OR'
    }
  }
];

export default APIMock;
